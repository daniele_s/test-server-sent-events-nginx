import express from "express";

const app = express();

app.get("/ssetest", async (req, res) => {
  res.set({
    "Cache-Control": "no-cache",
    "Content-Type": "text/event-stream",
    Connection: "keep-alive",
  });
  res.flushHeaders();

  res.write("retry: 10000\n\n");

  let count = 0;
  let connected = true;

  req.on("close", () => (connected = false));

  while (connected) {
    await new Promise((resolve) => setTimeout(resolve, 1000));

    console.log("Emit", ++count);
    // Emit an SSE that contains the current 'count' as a string
    res.write(`data: ${count}\n\n`);
  }
});

app.listen(8080, () => {
  console.log("Server is running on port 8080");
});
