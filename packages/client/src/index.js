const counter = document.getElementById("counter");

const evtSource = new EventSource("/ssetest");

evtSource.onmessage = (event) => {
  counter.innerHTML = event.data;
};
